$(document).on("ready", function() {
  $('.slid').slick({
  dots: true,
  infinite: false,
  speed: 400,
  slidesToShow: 4,
  slidesToScroll: 4,
  responsive: [
    {
      breakpoint: 680,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }
  ]
});
});
